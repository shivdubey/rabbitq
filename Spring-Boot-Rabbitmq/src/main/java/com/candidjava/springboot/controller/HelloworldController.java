package com.candidjava.springboot.controller;


import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.candidjava.springboot.publisher.Publisher;


import com.candidjava.springboot.dal.UserDAL;
import com.candidjava.springboot.dal.UserRepository;
import com.candidjava.springboot.model.User;


@RestController
@RequestMapping(value = "/user")
public class HelloworldController {
	
	private final UserRepository userRepository;

	private final UserDAL userDAL;

	public HelloworldController(UserRepository userRepository, UserDAL userDAL) {
		this.userRepository = userRepository;
		this.userDAL = userDAL;
	}

	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	//@RabbitListener(queues="${jsa.rabbitmq.queue}")
	public User addNewUsers(@RequestBody User user) {
	
	return userRepository.save(user);
	}
	
	
	@Autowired
	Publisher publisher;
	 User user= new User();
	
	  @RequestMapping("/send") public String sendMessage(@RequestParam("msg") String msg)
	  { 
		  System.out.println("*****"+msg); 
		  for(int i =0; i<5;i++)
	  {
	  publisher.produceMsg(msg); 
	//  User user = null;
	// user.setName("shiv");
	 //userRepository.save(user);
	  }
	  return "Successfully Msg Sent"; 
	  }
	 
	
		
		
		
	

}
