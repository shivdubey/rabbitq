   package com.candidjava.springboot.model;
   import java.util.Date;
	import java.util.HashMap;
	import java.util.Map;

	import org.springframework.data.annotation.Id;
	import org.springframework.data.mongodb.core.mapping.Document;

	@Document
	public class User {

		@Id
		private String userId;
		private String name;
		private Date creationDate = new Date();
		private Map<String, String> userSettings = new HashMap<>();

		public String getUserId() {
			return userId;
		}

		public void setUserId(String userId) {
			this.userId = userId;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}
}
