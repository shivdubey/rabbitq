package com.candidjava.springboot.subcriber;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import com.candidjava.springboot.dal.UserDAL;
import com.candidjava.springboot.dal.UserRepository;
import com.candidjava.springboot.model.User;
import com.candidjava.springboot.dal.UserDALIMPL;

import com.candidjava.springboot.model.User;
@Component
public class Subcriber {
	
	private final UserRepository userRepository;

	private final UserDAL userDAL;
	public   UserDALIMPL userdal = new UserDALIMPL();
	public   User user=new User() ;
	public String uname;
	public Subcriber(UserRepository userRepository, UserDAL userDAL) {
		this.userRepository = userRepository;
		this.userDAL = userDAL;
	}
	
	@RabbitListener(queues="${jsa.rabbitmq.queue}")
    public void recievedMessage(String msg) {
        System.out.println("Recieved Message  is : " + msg);
     //  userDAL
        
         uname=msg;
        //  public   User user ;
  	 user.setName(uname);
  	  System.out.println("user name is : " + user.getName());
  	  userRepository.save(user);
        userdal.addNewUserFromQ(msg);
        System.out.println("Recieved Message  is ---after : " + msg);
    }


}
