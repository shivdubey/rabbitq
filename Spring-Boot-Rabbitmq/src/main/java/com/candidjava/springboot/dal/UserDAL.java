package com.candidjava.springboot.dal;
import java.util.List;
import com.candidjava.springboot.model.User;

public interface UserDAL {
	
	User addNewUser(User user);
	void addNewUserFromQ(String msg);

}
